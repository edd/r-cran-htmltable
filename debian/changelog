r-cran-htmltable (2.4.3-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 26 Jul 2024 14:13:35 -0500

r-cran-htmltable (2.4.2-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Switch to virtual debhelper-compat (= 13)
  * debian/control: Correct case in Homepage: entry 	(Closes: #1037001)

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 03 Nov 2023 19:41:15 -0500

r-cran-htmltable (2.4.1-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to recent R version
  * debian/control: Set Standards-Version: to current version  

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 11 Jul 2022 14:34:12 -0500

r-cran-htmltable (2.4.0-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 04 Jan 2022 22:58:49 -0600

r-cran-htmltable (2.3.0-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 16 Oct 2021 22:46:22 -0500

r-cran-htmltable (2.2.1-2) unstable; urgency=medium

  * Simple rebuild for unstable following Debian 11 release

  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 16 Aug 2021 15:39:53 -0500

r-cran-htmltable (2.2.1-1) experimental; urgency=medium

  * New upstream release (to experimental during freeze)

  * debian/control: Set Build-Depends: to recent R version
  * debian/control: Set Standards-Version: to current version  

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 23 May 2021 16:27:02 -0500

r-cran-htmltable (2.1.0-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Switch to virtual debhelper-compat (= 11)
  * debian/compat: Removed

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 21 Sep 2020 18:02:34 -0500

r-cran-htmltable (2.0.1-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 07 Jul 2020 20:28:34 -0500

r-cran-htmltable (2.0.0-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 28 Jun 2020 15:53:56 -0500

r-cran-htmltable (1.13.3-2) unstable; urgency=medium

  * Rebuilt for r-4.0 transition

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 19 May 2020 08:45:08 -0500

r-cran-htmltable (1.13.3-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 10 Dec 2019 09:40:40 -0600

r-cran-htmltable (1.13.2-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 07 Oct 2019 20:55:01 -0500

r-cran-htmltable (1.13.1-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 17 Jan 2019 21:19:01 -0600

r-cran-htmltable (1.13-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 06 Jan 2019 11:32:38 -0600

r-cran-htmltable (1.12-2) unstable; urgency=medium

  * Rebuilding for R 3.5.0 transition
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Build-Depends: to 'debhelper (>= 10)'

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 06 Jun 2018 21:56:15 -0500

r-cran-htmltable (1.12-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Add Vcs-Browser: and Vcs-Git:
  * debian/control: Switch from cdbs to dh-r
  * debian/rules: Idem

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 29 May 2018 21:49:57 -0500

r-cran-htmltable (1.11.2-1) unstable; urgency=medium

  * New upstream release
  
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 22 Jan 2018 20:11:48 -0600

r-cran-htmltable (1.11.1-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 29 Dec 2017 13:08:49 -0600

r-cran-htmltable (1.11.0-1) unstable; urgency=medium

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  * debian/compat: Set level to 9

  * debian/control: Add new package r-cran-rstudioapi (as well as
    r-cran-dplyr, r-cran-tidyr) to (Build-)Depends

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 12 Dec 2017 07:16:09 -0600

r-cran-htmltable (1.9-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Add new (Build-)Depends: r-cran-htmlwidgets

  * debian/missing-sources/jquery.js: Added from source URL at
    https://code.jquery.com/jquery-3.1.1.js

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 31 Jan 2017 21:49:15 -0600

r-cran-htmltable (1.8-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 03 Jan 2017 13:18:43 -0600

r-cran-htmltable (1.7-2) unstable; urgency=medium

  * debian/README.sources: Added

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 09 Nov 2016 20:16:02 -0600

r-cran-htmltable (1.7-1) unstable; urgency=low

  * Initial Debian release 				(Closes: #843123)

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 04 Nov 2016 13:29:41 -0500
